<?php


namespace surface\api;

/**
 * reactive创建一个全局响应式对象
 * 仅创建一个响应式对象  如果需要绑创建并绑定 ["reactive:name" => [1,2,3]]
 */
class Reactive extends Api
{

    protected function type(): string
    {
        return 'reactive';
    }


}

