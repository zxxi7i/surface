<?php


namespace surface\api;

use surface\IFormat;

abstract class Api implements IFormat
{

    /**
     * @param string $name  全局的响应式对象对象名称 不存在时将会创建 支持深度绑定 a.b.c
     * @param mixed $value  默认值
     */
    public function __construct(public string $name = 'modelValue', public mixed $value = null){}

    abstract protected function type(): string;

    public function format(array &$config, mixed $name)
    {
        $config["{$this->type()}:{$name}:{$this->name}"] = $this->value;
        unset($config[$name]);
        return null;
    }

}

