<?php


namespace surface\api;

/**
 * 绑定v-model
 * 绑定格式 v-model:attr:name
 * 如果不存在 'name' 将自动注册ref响应式对象
 */
class Vmodel extends Api
{

    protected function type(): string
    {
        return 'v-model';
    }

}

