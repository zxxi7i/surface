<?php


namespace surface\api;

/**
 * ref创建一个全局响应式对象
 * 仅创建一个响应式对象  如果需要绑创建并绑定 ["ref:name" => "value"]
 */
class Ref extends Api
{

    protected function type(): string
    {
        return 'ref';
    }

}

