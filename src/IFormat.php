<?php

namespace surface;

/**
 * 代码格式化
 *
 * Interface IFormat
 *
 * @package surface
 */
interface IFormat {

    /**
     * @param array $config 当前配置
     * @param mixed $name   当前字段
     * @return mixed
     */
    public function format(array &$config, mixed $name);

}

